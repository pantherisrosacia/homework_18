﻿#include <cassert> // для assert
#include <iostream>
#include <stack>
#include <iomanip> // для setw
#include <string> 

template <typename T>
class Stack
{
private:
    T* stackPtr;                    
    int size;                         
    int top;                         
public:
    Stack(int = 10);                

    inline void push(const T&);     
    inline T pop();                  
    inline void printStack();   
    ~Stack();                        
};


template <typename T>
Stack<T>::Stack(int maxSize) :
    size(maxSize) 
{
    stackPtr = new T[size]; 
    top = 0; 
}


template <typename T>
inline void Stack<T>::push(const T& value)
{
    
    assert(top < size); 

    stackPtr[top++] = value; 
}


template <typename T>
inline T Stack<T>::pop()
{
  
    assert(top > 0); 

    return stackPtr[--top];
}


template <typename T>
inline void Stack<T>::printStack()
{
    for (int ix = top - 1; ix >= 0; ix--)
        std::cout << "|" << std::setw(4) << stackPtr[ix] << std::endl;
}

template <typename T>

Stack<T>::~Stack()
{
    delete[] stackPtr; 
}

int main()
{
    setlocale(LC_ALL, "Russian");
    int ct = 0;

    Stack<std::string> stackstr(5);
 
    while (ct++ < 5)
    {
        std::string temp;
        std::cin >> temp;
        stackstr.push(temp);
    }
    stackstr.printStack();

    std::cout << "\n Удаление элемента\n";
    stackstr.pop();
    stackstr.printStack();
    std::cout << "\n Добавление элемента\n";
    stackstr.push("test");
    stackstr.printStack();
    stackstr.~Stack();
}
